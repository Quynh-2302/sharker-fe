import React, { useEffect, useState } from "react";
import "./Home2.css";

const classname = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

export default function Home2(introduce) {
  const [introduces, setContents] = useState([]);

  useEffect(() => {
    console.log(introduce.introduce[0]);
    setContents(introduce.introduce[0]);
  }, [introduce]);

  return (
    <div className="home2">
      <div className="home2_introduce1">
        <h3
          style={{
            fontSize: "40px",
            lineHeight: "125%",
            color: "#48335E",
          }}
        >
          Học Với CEETUTOR
        </h3>
        <p>
          Với các phương thức và khoá học đa dạng, Marathon Education đáp ứng
          nhu cầu học tập của từng học sinh với cam kết kết quả tốt nhất.
        </p>
        <div className="home2_introduce1_1">
          {introduces.map((intro, key) => {
            return (
              <div className="home2_introduce1_block" key={key}>
                <img
                  className="home2_introduce1_img"
                  src={intro.img}
                  alt={intro.title}
                />
                <h4
                  style={{
                    color: "#486241",
                  }}
                >
                  {intro.title}
                </h4>
                <p>{intro.content}</p>
              </div>
            );
          })}
        </div>
      </div>
      <div className="home2_introduce2">
        <h3>Chương Trình Theo Bộ Giáo Dục</h3>
        <p>
          Marathon biên soạn bài giảng bám sát với chương trình của Bộ Giáo Dục,
          giúp học viên đạt thành tích học tập cao nhất.
        </p>
        <div className="home2_introduce2_1">
          {classname.map((classnames, key) => {
            if (classnames <= 5) {
              return (
                <div
                  key={key}
                  className="home2_introduce2_1_block"
                  style={{
                    backgroundColor: "rgba(248,222,205)",
                  }}
                >
                  Lớp {classnames}
                </div>
              );
            }
            if (classnames <= 9) {
              return (
                <div
                  key={key}
                  className="home2_introduce2_1_block"
                  style={{
                    backgroundColor: "rgba( 241,156,187)",
                  }}
                >
                  Lớp {classnames}
                </div>
              );
            } else {
              return (
                <div
                  key={key}
                  className="home2_introduce2_1_block"
                  style={{
                    backgroundColor: "rgba(255,183,197)",
                  }}
                >
                  Lớp {classnames}
                </div>
              );
            }
          })}
        </div>
      </div>
    </div>
  );
}
