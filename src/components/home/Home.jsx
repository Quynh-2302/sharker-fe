import React from "react";
import "./Home.css";
import Home1 from "./Home1";
import { IMG } from "../../assets";
import Home2 from "./Home2";
import Ielts from "./Ielts";
import Slide from "./Slide";

const content = [
  {
    imgs: IMG.laptop,
    text1: "Nền Tảng Học Tập",
    text2: "Tương Tác Trực Tuyến",
  },
  {
    imgs: IMG.medal,
    text1: "Thầy Cô Và Cộng Đồng",
    text2: "Học Tập Dẫn Đầu",
  },
  {
    imgs: IMG.board,
    text1: "Khóa Học Đa Dạng",
    text2: "Theo Nhu Cầu",
  },
  {
    imgs: IMG.calendar,
    text1: "Cam Kết Lộ Trình",
    text2: "Kết Quả Rõ Ràng",
  },
];
const introduce = [
  {
    img: IMG.book,
    title: "Học Với Giáo Viên",
    content:
      "Học bổ trợ các môn học theo chương trình Bộ GD với các thầy cô top 1%",
  },
  {
    img: IMG.bag,
    title: "Học Với Gia Sư",
    content: "Học gia sư 1 kèm 1, Học gia sư 1 kèm nhóm",
  },
  {
    img: IMG.document,
    title: "Học Luyện Thi",
    content: "Đánh giá năng lực, Luyện thi vào lớp 10, Luyện thi đại học",
  },
  {
    img: IMG.ielts,
    title: "Học IELTS Với GV Bản Ngữ",
    content:
      "Giúp học sinh từ mất gốc tiếng Anh đạt đến 8.0 IELTS với lộ trình tinh gọn 4 cấp độ.",
  },
  {
    img: IMG.dev,
    title: "Khoá Học Lập Trình Cho Trẻ Em",
    content: "Học kiến thức lập trình, làm game từ cơ bản cho trẻ em",
  },
  {
    img: IMG.computer,
    title: "Khoá Học Lập Trình Người Lớn",
    content:
      "Học kiến thức lập trình chuyên nghiệp dành cho sinh viên và người đi làm",
  },
];
const images = [
  {
    link_name: "/#",
    img_slide: IMG.slide1,
  },
  {
    link_name: "/#",
    img_slide: IMG.slide2,
  },
  {
    link_name: "/#",
    img_slide: IMG.slide3,
  },
  {
    link_name: "/#",
    img_slide: IMG.slide4,
  },
  {
    link_name: "/#",
    img_slide: IMG.slide5,
  },
  {
    link_name: "/#",
    img_slide: IMG.slide6,
  },
  {
    link_name: "/#",
    img_slide: IMG.slide7,
  },
];
export default function Home() {
  return (
    <>
      <Home1 content={[content]} />
      <Home2 introduce={[introduce]} />
      <Ielts />
      <Slide images={[images]} />
    </>
  );
}
