import React, { useEffect, useState } from "react";
import { IMG } from "../../assets";
import { Button } from "@mui/material";
import "./Home1.css";

export default function Home1(content) {
  const [contents, setContents] = useState([]);
  useEffect(() => {
    setContents(content.content[0]);
  }, [content]);

  return (
    <>
      <div className="home1">
        <div className="introduce1">
          <div
            style={{
              maxWidth: "40%",
            }}
          >
            <div>
              <h1>
                <span
                  style={{
                    fontSize: "50px",
                    lineHeight: "125%",
                    color: "#48335E",
                  }}
                >
                  CEETUTOR
                </span>
                <br />
                <span
                  style={{
                    fontSize: "39px",
                    lineHeight: "125%",
                    color: "#486241",
                  }}
                >
                  Nền tảng học trực tuyến
                </span>
              </h1>
              <span
                style={{
                  fontSize: "25px",
                  lineHeight: "125%",
                  color: "#BCB19F",
                }}
              >
                Nền tảng học online Marathon Education cải thiện và nâng cao học
                lực của bạn với nhiều loại hình giảng dạy kết hợp với công nghệ
                thu thập và xử lý dữ liệu theo từng buổi học.
              </span>
            </div>
            <Button
              style={{
                backgroundColor: "#486241",
                color: "#fff",
                margin: "20px 0",
                position: "inherit",
              }}
            >
              Tìm hiểu thêm thông tin
            </Button>
          </div>
          <div>
            <img src={IMG.imgVP} style={{ width: "100%" }} alt="ảnh" />
          </div>
        </div>
        <div className="introduce2">
          {contents.map((items, key) => {
            return (
              <div key={key} className="introduce2_1">
                <img src={items.imgs} alt="a" />
                <p>{items.text1}</p>
                <p>{items.text2}</p>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
}
