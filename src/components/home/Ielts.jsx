import React from "react";
import { Button } from "@mui/material";
import "./Ielts.css";

const prop = [
  {
    point: "A2",
    name: "ielts ignite",
    name_qt: "Nền tảng",
    scores: "3.5",
    colors: "rgba(86, 42, 118, 0.4)",
    margin_top: "9rem",
    ddv: "<3.5 (A0,A1)",
    gt: "Skilful F và Skillful 1",
    colorPoint: "rgba(86, 42, 118)",
    box_shadow:
      "rgb(86, 42, 118) -4px 4px 0px 0px, rgb(189, 189, 189) -20px 20px 30px 0px",
  },
  {
    point: "B1",
    name: "ielts depart",
    name_qt: "Khởi hành",
    scores: "4.0 - 5.0",
    colors: "rgba(116, 197, 229, 0.4)",
    margin_top: "6rem",
    ddv: "3.5 (A2)",
    gt: "Skilful 2 và Mindset for IELTS 2",
    colorPoint: "rgba(116, 197, 229)",
    box_shadow:
      "rgb(116, 197, 229) -4px 4px 0px 0px, rgb(189, 189, 189) -20px 20px 30px 0px",
  },
  {
    point: "B2",
    name: "ielts accelerate",
    name_qt: "Tăng tốc",
    scores: "5.5 - 6.5",
    colors: "rgba(228, 128, 67, 0.4)",
    margin_top: "3rem",
    ddv: "4.0 - 5.0 (A2)",
    gt: "Skilful 3 và Mindset for IELTS 2",
    colorPoint: "rgba(228, 128, 67)",
    box_shadow:
      "rgb(228, 128, 67) -4px 4px 0px 0px, rgb(189, 189, 189) -20px 20px 30px 0px",
  },
  {
    point: "C1",
    name: "ielts beyond",
    name_qt: "Vươn xa",
    scores: "7.0 - 8.0",
    colors: "rgba(118, 190, 67, 0.4)",
    margin_top: "0rem",
    ddv: "5.5 - 6.5 (B2)",
    gt: "Skilful 4 và Mindset for IELTS 3",
    colorPoint: "rgba(118, 190, 67  )",
    box_shadow:
      "rgb(118, 190, 67) -4px 4px 0px 0px, rgb(189, 189, 189) -20px 20px 30px 0px",
  },
];
export default function Ielts(props) {
  return (
    <div
      style={{
        backgroundColor: "rgba(232, 225, 230, 0.3)",
      }}
    >
      <div className="ielt">
        <div className="introduct_ielt">
          <h3 className="introduct_ielt_1">
            <span
              style={{
                fontSize: "50px",
                lineHeight: "125%",
                color: "#48335E",
              }}
            >
              Học IELTS Với
            </span>
            <br />
            <span
              style={{
                fontSize: "50px",
                lineHeight: "125%",
                color: "#486241",
              }}
            >
              Giáo Viên Bản Ngữ
            </span>
          </h3>
          <div className="introduct_ielt_2">
            <p
              style={{
                fontSize: "24px",
                lineHeight: "120%",
                color: "#BCB19F",
              }}
            >
              Khóa học IELTS với giáo viên bản ngữ được Marathon thiết kế độc
              quyền, xây dựng kiến thức nền tảng đến nâng cao, giúp học sinh làm
              quen chiến lược thi và rèn luyện các kĩ năng Nghe - Nói - Đọc -
              Viết hiệu quả.
            </p>
            <div>
              <Button
                style={{
                  position: "inherit",
                }}
              >
                Danh sách khóa học
              </Button>
              <Button
                style={{
                  position: "inherit",
                }}
              >
                Danh sách giáo viên
              </Button>
            </div>
          </div>
        </div>
        <div>
          <div className="diagram_ielt">
            {prop.map((item, k) => {
              return (
                <div
                  className="ignite"
                  key={k}
                  style={{ marginTop: item.margin_top }}
                >
                  <div
                    className="a2"
                    style={{
                      color: item.colorPoint,
                      boxShadow: item.box_shadow,
                    }}
                  >
                    {item.point}
                  </div>
                  <div className="duongvach"></div>
                  <div
                    className="i_ignite"
                    style={{ backgroundColor: item.colors }}
                  >
                    <p className="i_ignite_p">{item.name}</p>
                    <p className="i_ignite_p2">{item.name_qt}</p>
                    <p className="i_ignite_p3">{item.scores}</p>
                  </div>
                  <ul>
                    <li>
                      Điểm đầu vào:{" "}
                      <span
                        style={{
                          fontWeight: "bold",
                          color: "purple",
                          fontSize: "15px",
                        }}
                      >
                        {item.ddv}
                      </span>
                    </li>
                    <li>
                      Giáo trình:{" "}
                      <span
                        style={{
                          fontWeight: "bold",
                          color: "purple",
                          fontSize: "15px",
                        }}
                      >
                        {item.gt}
                      </span>
                    </li>
                  </ul>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
