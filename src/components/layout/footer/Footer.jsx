import React from "react";
import "./Footer.css";
import { Button } from "@mui/material";

export default function Footer() {
  return (
    <div className="footer">
      <div className="footer_dl">
        <div className="lh">
          <h4>Liên hệ</h4>
          <p>
            Khi có thắc mắc hoặc khiếu nại về chất lượng dịch vụ, vui lòng liên
            hệ:
          </p>
          <p>
            <b>Hotline:</b> (035) 730 303
          </p>
          <p>
            <b>Email:</b> nptvu.19it5@vku.udn/vn
          </p>
          <p>
            Công ty TNHH ..... Giấy CNĐKDN số: 015, ngày cấp 18/8/2021 bởi Sở Kế
            hoạch và Đầu tư Thành Phố Hồ Chí Minh
          </p>
        </div>
        <div className="web">
          <h4>Về Website</h4>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Giới thiệu
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Phòng truyền thống
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Học sinh tiểu học
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Bản tin
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Tài liệu học tập
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Liên hệ
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Tuyển dụng
          </Button>
        </div>
        <div className="cs_ht">
          <h4>Chính sách & Hỗ trợ</h4>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Quy chế hoạt động
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Chính sách & quy định chung
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Chính sách bảo mật
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Điều khoản sử dụng
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Giải đáp thắc mắc
          </Button>
        </div>
        <div className="cth">
          <h4>Chương trình học tiêu biểu</h4>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Gia sư trực tuyến
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Học toán online lớp 9
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Học toán online lớp 8
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Học toán online lớp 7
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Học toán online lớp 6
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Học online lớp 6
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Học online lớp 7
          </Button>
          <Button
            sx={{
              color: "#424242;",
              fontSize: "13px",
              margin: "2px",
              padding: 0,
            }}
          >
            Các khoá luyện thi
          </Button>
        </div>
      </div>
    </div>
  );
}
