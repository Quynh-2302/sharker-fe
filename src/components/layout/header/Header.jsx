import React from "react";
import "./Header.css";
import logo from "../../../assets/image/Safehorizons.png";
import { Link } from "react-router-dom";

const pages = [
  {
    name: "Khóa học",
    link: "mon-hoc",
  },
  {
    name: "Giáo viên",
    link: "giao-vien",
  },
  {
    name: "Ngân hàng đề thi",
    link: "",
  },
  {
    name: "Hỏi đáp AI",
    link: "",
  },
  {
    name: "Bản tin Ceetutor",
    link: "",
  },
];
export default function Header() {
  return (
    <>
      <header id="mainMenu">
        <Link to="/">
          <img src={logo} alt="logo" className="imglogo" />
        </Link>
        <div className="drop_menu">
          {pages.map((page, key) => (
            <Link to={page.link} className="nav_items">
              {page.name}
            </Link>
          ))}
        </div>
      </header>
    </>
  );
}
