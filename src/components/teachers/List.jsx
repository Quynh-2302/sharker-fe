import React from "react";
import PropTypes from "prop-types";
import { Tabs, Tab, Typography, Box } from "@mui/material";
import "./Giaovien.css";
import { IMG } from "../../assets";

const classNames = [
  { name: "Tất cả" },
  { name: "Lớp 1" },
  { name: "Lớp 2" },
  { name: "Lớp 3" },
  { name: "Lớp 4" },
  { name: "Lớp 5" },
  { name: "Lớp 6" },
  { name: "Lớp 7" },
  { name: "Lớp 8" },
  { name: "Lớp 9" },
  { name: "Lớp 10" },
  { name: "Lớp 11" },
  { name: "Lớp 12" },
];
const classGV = [
  {
    ten: "Nguyễn Văn A",
    mon: "Tiếng Anh",
    img_gv: IMG.gv_nam,
    lop: 12,
  },
  {
    ten: "Nguyễn Văn D",
    mon: "Toán",
    img_gv: IMG.gv_nu,
    lop: 12,
  },
  {
    ten: "Nguyễn Văn C",
    mon: "Tiếng Anh",
    img_gv: IMG.gv_nam,
    lop: 11,
  },
  {
    ten: "Nguyễn Văn D",
    mon: "Toán",
    img_gv: IMG.gv_nu,
    lop: 11,
  },
  {
    ten: "E",
    mon: "Tiếng Anh",
    img_gv: IMG.gv_nam,
    lop: " 10",
  },
  {
    ten: "G",
    mon: "Toán",
    img_gv: IMG.gv_nu,
    lop: 10,
  },
  {
    ten: "Nguyễn Văn C",
    mon: "Tiếng Anh",
    img_gv: IMG.gv_nam,
    lop: 9,
  },
  {
    ten: "Nguyễn Văn D",
    mon: "Toán",
    img_gv: IMG.gv_nu,
    lop: 9,
  },
  {
    ten: "Nguyễn Văn C",
    mon: "Tiếng Anh",
    img_gv: IMG.gv_nam,
    lop: 8,
  },
  {
    ten: "Nguyễn Văn D",
    mon: "Toán",
    img_gv: IMG.gv_nu,
    lop: 8,
  },
  {
    ten: "Nguyễn Văn C",
    mon: "Tiếng Anh",
    img_gv: IMG.gv_nam,
    lop: 7,
  },
  {
    ten: "Nguyễn Văn D7",
    mon: "Toán",
    img_gv: IMG.gv_nu,
    lop: 7,
  },
  {
    ten: "Nguyễn Văn C6",
    mon: "Tiếng Anh",
    img_gv: IMG.gv_nam,
    lop: 6,
  },
  {
    ten: "Nguyễn Văn D",
    mon: "Toán",
    img_gv: IMG.gv_nu,
    lop: 6,
  },
  {
    ten: "Nguyễn Văn C",
    mon: "Tiếng Anh",
    lop: 5,
    img_gv: IMG.gv_nam,
  },
  {
    ten: "Nguyễn Văn D",
    mon: "Toán",
    lop: 5,
    img_gv: IMG.gv_nu,
  },
  {
    ten: "Nguyễn Văn C",
    mon: "Tiếng Anh",
    lop: 4,
    img_gv: IMG.gv_nam,
  },
  {
    ten: "Nguyễn Văn D",
    mon: "Toán",
    lop: 4,
    img_gv: IMG.gv_nu,
  },
  {
    ten: "Nguyễn Văn C",
    mon: "Tiếng Anh",
    img_gv: IMG.gv_nam,
    lop: 3,
  },
  {
    ten: "Nguyễn Văn D",
    mon: "Toán",
    img_gv: IMG.gv_nu,
    lop: 3,
  },
  {
    ten: "Nguyễn Văn C",
    mon: "Tiếng Anh",
    img_gv: IMG.gv_nam,
    lop: 2,
  },
  {
    ten: "Nguyễn Văn D",
    mon: "Toán",
    img_gv: IMG.gv_nu,
    lop: 2,
  },
  {
    ten: "Nguyễn Văn C 1",
    mon: "Tiếng Anh",
    img_gv: IMG.gv_nam,
    lop: 1,
  },
  {
    ten: "Nguyễn Văn D",
    mon: "Toán",
    img_gv: IMG.gv_nu,
    lop: 1,
  },
];
function CustomTabPanel(props) {
  const { children, value, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};
export default function List() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="gv">
      <Box sx={{ maxWidth: "1200px", width: "100%" }}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            {classNames.map((v, key) => {
              // console.log(v.name);
              console.log(value);
              return <Tab key={key} label={v.name} />;
            })}
          </Tabs>
        </Box>

        <CustomTabPanel value={value} index={value}>
          <div className="gv_dl">
            {classGV.map((v, key) => {
              if (value === 0) {
                return (
                  <div className="gv_dls" key={key}>
                    <div className="backimg">
                      <img
                        src={v.img_gv}
                        alt="v.ten"
                        style={{
                          width: "220px",
                          height: "220px",
                          objectFit: "contain",
                        }}
                      />
                    </div>
                    <p className="gv_mon">{v.mon}</p>

                    <p>- Cử nhân -</p>
                    <p>{v.ten}</p>
                  </div>
                );
              } else if (value === v.lop) {
                return (
                  <div className="gv_dls" key={key}>
                    <div className="backimg">
                      <img
                        src={v.img_gv}
                        alt="v.ten"
                        style={{
                          width: "220px",
                          height: "220px",
                          objectFit: "contain",
                        }}
                      />
                    </div>
                    <p className="gv_mon">{v.mon}</p>

                    <p>- Cử nhân -</p>
                    <p>{v.ten}</p>
                  </div>
                );
              }
            })}
          </div>
        </CustomTabPanel>
      </Box>
    </div>
  );
}
