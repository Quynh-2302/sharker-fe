import React from "react";
import { IMG } from "../../assets";
import { Button } from "@mui/material";
import "./IntroductGV.css";

export default function IntroductGV() {
  return (
    <div className="gv1">
      <div className="introducegv1">
        <div>
          <img src={IMG.imgVP} style={{ width: "100%" }} alt="ảnh" />
        </div>
        <div
          style={{
            maxWidth: "40%",
          }}
        >
          <div>
            <h1>
              <span
                style={{
                  fontSize: "50px",
                  lineHeight: "125%",
                  color: "#48335E",
                }}
              >
                CEETUTOR
              </span>
              <br />
              <span
                style={{
                  fontSize: "39px",
                  lineHeight: "125%",
                  color: "#486241",
                }}
              >
                Nền tảng học trực tuyến
              </span>
            </h1>
            <span
              style={{
                fontSize: "25px",
                lineHeight: "125%",
                color: "#BCB19F",
              }}
            >
              Nền tảng học online Marathon Education cải thiện và nâng cao học
              lực của bạn với nhiều loại hình giảng dạy kết hợp với công nghệ
              thu thập và xử lý dữ liệu theo từng buổi học.
            </span>
          </div>
          <Button
            style={{
              backgroundColor: "#486241",
              color: "#fff",
              margin: "20px 0",
              position: "inherit",
            }}
          >
            Tìm hiểu thêm thông tin
          </Button>
        </div>
      </div>
    </div>
  );
}
