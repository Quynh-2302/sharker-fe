import React from "react";
import IntroductGV from "./IntroductGV";
import ListGV from "./List";

export default function Giaovien() {
  return (
    <>
      <IntroductGV />
      <ListGV />
    </>
  );
}
