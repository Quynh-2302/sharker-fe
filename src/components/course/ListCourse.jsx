import React, { useState } from "react";
import PropTypes from "prop-types";
import { Tabs, Tab, Typography, Box } from "@mui/material";
import "./ListCourse.css";
import { IMG } from "../../assets";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import Collapse from "@mui/material/Collapse";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";

const classNames = [
  { name: "Tất cả" },
  { name: "Lớp 1" },
  { name: "Lớp 2" },
  { name: "Lớp 3" },
  { name: "Lớp 4" },
  { name: "Lớp 5" },
  { name: "Lớp 6" },
  { name: "Lớp 7" },
  { name: "Lớp 8" },
  { name: "Lớp 9" },
  { name: "Lớp 10" },
  { name: "Lớp 11" },
  { name: "Lớp 12" },
];
const classmon = [
  {
    lop: 12,
    open: true,
    danhsach: [
      {
        ten: "Toán 12",
        nam: "2023-2024",
        img_mon: IMG.toan,
      },
      {
        ten: "Ngữ Văn 12",
        nam: "2023-2024",
        img_mon: IMG.nguvan,
      },
      {
        ten: "Tiếng Anh 12",
        nam: "2023-2024",
        img_mon: IMG.t_anh,
      },
    ],
  },
  {
    lop: 11,
    open: true,
    danhsach: [
      {
        ten: "Ngữ Văn 11",
        nam: "2023-2024",
        img_mon: IMG.t_anh,
      },
      {
        ten: "Toán 11",
        nam: "2023-2024",
        img_mon: IMG.t_anh,
      },
      {
        ten: "Tiếng Anh 11",
        nam: "2023-2024",
        img_mon: IMG.t_anh,
      },
    ],
  },
];
function CustomTabPanel(props) {
  const { children, value, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

export default function ListCourse() {
  const [value, setValue] = useState(0);
  const [classmonData, setClassmonData] = useState(classmon);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleCollapse = (classIndex) => {
    const updatedData = [...classmonData];
    updatedData[classIndex].open = !updatedData[classIndex].open;
    setClassmonData(updatedData);
  };

  return (
    <div className="gen_mon">
      <Box sx={{ maxWidth: "1200px", width: "100%" }}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            {classNames.map((v, key) => {
              return <Tab key={key} label={v.name} />;
            })}
          </Tabs>
        </Box>

        <CustomTabPanel value={value} index={value}>
          <div className="d_mon">
            {classmonData.map((v, lop) => {
              console.log("v", v);
              console.log("classIndex:", lop);
              return (
                <div key={v.lop}>
                  <ListItemButton onClick={() => handleCollapse(lop)}>
                    <ListItemText primary={"Lớp " + v.lop} />
                    {v.open ? <ExpandLess /> : <ExpandMore />}
                  </ListItemButton>
                  <Collapse in={v.open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      <div className="d_m_b">
                        {v.danhsach.map((mon, key) => {
                          return (
                            <div className="d_mons" key={key}>
                              <div>
                                <img
                                  src={mon.img_mon}
                                  alt="v.ten"
                                  style={{
                                    width: "200px",
                                    height: "200px",
                                    objectFit: "contain",
                                  }}
                                />
                              </div>
                              <div className="d_mon_n">
                                <div className="d_mon_n1">
                                  <p className="bgd">Bộ giáo dục</p>
                                  <p className="dmon_lop">
                                    Lớp
                                    <span
                                      style={{
                                        fontWeight: 600,
                                        fontSize: "16px",
                                        display: "block",
                                        color: "rgb(66, 66, 66)",
                                      }}
                                    >
                                      {v.lop}
                                    </span>
                                  </p>
                                </div>
                                <p style={{ fontSize: "12px" }}>{mon.nam}</p>
                                <p
                                  style={{
                                    margin: "10px 0px",
                                    fontSize: "20px",
                                    fontWeight: "bold",
                                  }}
                                >
                                  {mon.ten}
                                </p>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </List>
                  </Collapse>
                </div>
              );
            })}
          </div>
        </CustomTabPanel>
      </Box>
    </div>
  );
}
