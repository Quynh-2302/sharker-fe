import imgVP from "./image/img_vp.jpg";
import laptop from "./image/laptop.jpg";
import medal from "./image/medal.jpg";
import board from "./image/board.jpg";
import calendar from "./image/cal.jpg";
import book from "./image/book.jpg";
import bag from "./image/bag.jpg";
import document from "./image/document.jpg";
import ielts from "./image/IELTS.jpg";
import dev from "./image/dev.jpg";
import computer from "./image/computer.jpg";
import slide1 from "./image/middle-banner-codi-pc.png";
import slide2 from "./image/middle-banner-eil-pc.png";
import slide3 from "./image/middle-banner-pc-3.png";
import slide4 from "./image/middle-banner-promo-pc.png";
import slide5 from "./image/middle-banner-promo2-pc.png";
import slide6 from "./image/middle-banner-promo3-pc.png";
import slide7 from "./image/middle-banner-trial-pc.jpeg";
import gv_nam from "./image/GV_boy.png";
import gv_nu from "./image/GV_girl.png";
import toan from "./image/monhoc/toan.png";
import nguvan from "./image/monhoc/nguvan.png";
import t_anh from "./image/monhoc/tanh.png";
import hoahoc from "./image/monhoc/hoahocj.png";
import sinhhoc from "./image/monhoc/sinhhoc.png";
import vatly from "./image/monhoc/vatly.png";

const IMG = {
  toan,
  nguvan,
  t_anh,
  hoahoc,
  sinhhoc,
  vatly,
  gv_nam,
  gv_nu,
  imgVP,
  laptop,
  board,
  calendar,
  medal,
  book,
  bag,
  computer,
  dev,
  document,
  ielts,
  slide1,
  slide2,
  slide3,
  slide4,
  slide5,
  slide6,
  slide7,
};

export default IMG;
