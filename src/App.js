import Home from "./components/home/Home";
import Footer from "./components/layout/footer/Footer";
import Header from "./components/layout/header/Header";

import { Route, Routes } from "react-router-dom";
import Giaovien from "./components/teachers/Giaovien";
import Course from "./components/course/Course";

function App() {
  return (
    <>
      <Header />
      <div
        style={{
          marginTop: "100px",
        }}
      >
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/giao-vien" element={<Giaovien />} />
          <Route path="/mon-hoc" element={<Course />} />
        </Routes>
      </div>

      <Footer />
    </>
  );
}

export default App;
